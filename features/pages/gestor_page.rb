class Gestor < SitePrism::Page

element :menu, "a[data-test-id='elo-navbar-item-school-manager']"
element :campo, "input[data-test='search-box-input']"
element :selecao, "li[data-test='search-box-item-1']"
element :ano,      "input[class='css-38lglc']" 


    def acessar_gestor
    menu.click
     
    end
    
    def buscar_escola
        campo.click
    end

    def selecionar_escola
        selecao.click
    
    end

    def anoletivo_escola
        ano.click
    end
end 

